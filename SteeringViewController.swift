//
//  SteeringViewController.swift
//  Bluetooth
//
//  Created by Szymon Dawidow on 30.12.2016.
//  Copyright © 2016 Szymon Dawidow. All rights reserved.
//

import UIKit

class SteeringViewController: DeviceDetailViewController {

    @IBOutlet weak var leftEngineSlider: UISlider!
    @IBOutlet weak var rightEngineSlider: UISlider!
    @IBOutlet weak var webView: UIWebView!
    var currentValue:Int  = 0
    var cameraUrl:String = "http://imageserver.webcamera.pl/umiesc/krakow4"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.delegate = self
        leftEngineSlider.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
        rightEngineSlider.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
        loadCameraUrl(url: cameraUrl )
    }
    
    @IBAction func engineSliderValueChanged(_ sender: UISlider) {
        let rounded = round(sender.value)
        sender.setValue(round(sender.value), animated: true)
        var value:Int = 0;
        
        if sender == leftEngineSlider {
            value = enginesValue(left: rounded, right: rightEngineSlider.value)
            if value == currentValue {
                return
            }
        }
    
        if sender == rightEngineSlider {
            value = enginesValue(left: leftEngineSlider.value, right: rounded)
            if value == currentValue {
                return
            }
        }
            self.device?.writeValue("\(value)".data(using: String.Encoding.utf8)!, for: self.characteristic!, type: .withoutResponse)
            //self.device?.readValue(for: self.characteristic!)
            print("\nEngines value =  \(value)")
            print("     left \(leftEngineSlider.value)")
            print("     right\(rightEngineSlider.value)")
            currentValue = value
    }
    
    func enginesValue() -> Int {
        return (Int(self.leftEngineSlider.value)*3 + Int(self.rightEngineSlider.value))
    }
    
    func enginesValue(left: Float, right: Float) -> Int {
        return (Int(left)*3 + Int(right))
    }
    
    
    func loadCameraUrl(url: String) {
        let request = URLRequest(url: URL(string: url)!)
        self.webView.loadRequest(request)
        self.webView.reload()
    }
    
    @IBAction func networkAction(_ sender: UIButton) {
        var textField: UITextField?
       
        
        let addIp = UIAlertAction(
        title: "Change", style: .default) {
            (action) -> Void in
            
            if let ip = textField?.text {
                self.cameraUrl = "http://\(ip)"
                self.loadCameraUrl(url: self.cameraUrl)
                print(self.cameraUrl)
                
            } else {
                print("No IP")
            }
        }
        
        
        let alert = UIAlertController(title: "IP", message: "Podaj adres IP", preferredStyle: .alert)
        
        
        alert.addTextField { (text) in
            textField = text
            //textField!.placeholder = text
        }
        
        
        alert.addAction(addIp)
        self.present(alert, animated: true, completion: nil)
        
    }
}
extension SteeringViewController : UIWebViewDelegate {
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error)
    }
}


