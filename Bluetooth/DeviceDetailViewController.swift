//
//  DeviceDetailViewController.swift
//  Bluetooth
//
//  Created by Szymon Dawidow on 26.11.2016.
//  Copyright © 2016 Szymon Dawidow. All rights reserved.
//

import UIKit
import CoreBluetooth

class DeviceDetailViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    var device: CBPeripheral?
    var characteristic: CBCharacteristic?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.device != nil {
            self.device!.delegate = self
            let serviceUUID = CBUUID(string: "FFE0")
            self.device?.discoverServices([serviceUUID])
        }

        //self.nameLabel.text = self.device?.name

    }
    
//MARK : - Actions
    @IBAction func discoverServicesButtonAction(_ sender: UIButton) {
        self.device!.delegate = self
        let serviceUUID = CBUUID(string: "FFE0")
        self.device?.discoverServices([serviceUUID])
    }
    @IBAction func writeAction(_ sender: UIButton) {
        let stringData = "9"
        self.device?.writeValue(stringData.data(using: String.Encoding.utf8)!, for: self.characteristic!, type: .withoutResponse)
        self.device?.readValue(for: self.characteristic!)
    }
}

//MARK : - Periferal delegate
extension  DeviceDetailViewController : CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        print("\nSERVICES : \(peripheral.services)")
        let characteristicUUID = CBUUID(string: "FFE1")
        peripheral.discoverCharacteristics([characteristicUUID], for: peripheral.services!.first!)
        
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        self.characteristic = service.characteristics?.first
        print("\n# Characteristics : \(self.characteristic))")
        peripheral.setNotifyValue(true, for: self.characteristic!)
        peripheral.readValue(for: self.characteristic!)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
       let data = characteristic.value
        //print("value =  \(data)")
        print("value =  \(String(data: characteristic.value!, encoding: String.Encoding.utf8))")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        
        if ((error) != nil) {
            NSLog("Error changing notification state: \(error?.localizedDescription)   ")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("value written")
        print(String(data: characteristic.value!, encoding: String.Encoding.utf8) ?? "Lama")
        print("\n# Characteristics : \(self.characteristic))")
        
        if ((error) != nil) {
            NSLog("Error writing to characteristic: \(error?.localizedDescription)   ")
            
        }
    }
}
