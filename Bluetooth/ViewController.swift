//
//  ViewController.swift
//  Bluetooth
//
//  Created by Szymon Dawidow on 20.11.2016.
//  Copyright © 2016 Szymon Dawidow. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController {
   
    @IBOutlet weak var tableView: UITableView!
    let manager = CBCentralManager()
    var devices: [CBPeripheral] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.manager.delegate = self
    }

}


//MARK : - Bluetooth Central
extension ViewController : CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
        case .poweredOff:
            print("CoreBluetooth BLE hardware is powered off")
        case .poweredOn:
            print("CoreBluetooth BLE hardware is powered on")
            self.manager.scanForPeripherals(withServices: nil, options: nil)
        case .resetting:
            print("CoreBluetooth BLE hardware is resetting")
        case .unauthorized:
            print("CoreBluetooth BLE state is unauthorized")
        case .unknown:
            print("CoreBluetooth BLE state is unknown")
        case .unsupported:
            print("CoreBluetooth BLE hardware is unsupported on this platform")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let discoveredDevice = peripheral;
        if !self.devices.contains(discoveredDevice) {
            
            self.devices.append(discoveredDevice)
            self.tableView.reloadData()
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Successfully connected to \(peripheral)")
        //peripheral.delegate = self
        let uuid = CBUUID(string: "9FA480E0-4967-4542-9390-D343DC5D04AE")
      //  peripheral.discoverServices(nil)
     
        
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Connection to  \(peripheral) failed")
    }
    
    
}


//MARK : - Periferal delegate

extension ViewController : CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        print("\nSERVICES : \(peripheral.services)")
        peripheral.discoverCharacteristics(nil, for: peripheral.services!.first!)
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("\n'nCharacteristics : \(service.characteristics))")
    }
}

//MARK : - Table View
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.devices.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as UITableViewCell
        
        if indexPath.row == 0 {
            cell.textLabel?.text = "Count : \(self.devices.count)"
            return cell
        }
        
            let currentDevice: CBPeripheral = self.devices[indexPath.row - 1]
            cell.textLabel?.text = (currentDevice.name != nil ? currentDevice.name : "Name unavialable")
            cell.backgroundColor = UIColor.green
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            self.tableView.deselectRow(at: indexPath, animated: true)
            let currentDevice = self.devices[indexPath.row-1]
            self.manager.connect(currentDevice, options: nil)
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "deviceDetail") as! DeviceDetailViewController
//            vc.device = currentDevice
//            self.navigationController?.pushViewController(vc, animated: true)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "steering") as! SteeringViewController
            vc.device = currentDevice
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
}
